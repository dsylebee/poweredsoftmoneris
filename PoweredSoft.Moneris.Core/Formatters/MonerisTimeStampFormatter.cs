﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace PoweredSoft.Moneris.Core.Formatters
{
    public class MonerisTimeStampFormatter : IMessagePartFormatter
    {
        public string Format(MessagePart part, string value)
        {
            var currentYearFirstTwoNumbers = $"{new string(DateTime.Now.Year.ToString().Take(2).ToArray())}";
            if (DateTime.TryParseExact($"{currentYearFirstTwoNumbers}{value}", $"yyyyMMddHHmmss", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out var dt))
                return $"{dt}";

            return "";
        }
    }
}
