﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PoweredSoft.Moneris.Core.Formatters
{
    public class MonerisAmountFormatter : IMessagePartFormatter
    {
        public string Format(MessagePart part, string value)
        {
            if (!string.IsNullOrWhiteSpace(value) && long.TryParse(value, out var intVal))
                return $"{(decimal)intVal / 100.00M:c2}";

            return string.Empty;
        }
    }
}
