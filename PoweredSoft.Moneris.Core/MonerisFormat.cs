﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PoweredSoft.Moneris.Core
{ 
    public class MessageFormat
    {
        public string Title { get; set; }

        public List<MessagePart> Parts { get; set; }

        public MessageFormat()
        {
            Initialize();
        }

        public virtual void Initialize()
        {
            
        }
    }
}
