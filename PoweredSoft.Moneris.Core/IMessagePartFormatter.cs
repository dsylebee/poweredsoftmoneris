﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PoweredSoft.Moneris.Core
{
    public interface IMessagePartFormatter
    {
        string Format(MessagePart part, string value);
    }

    public class DefaultMessagePartFormatter : IMessagePartFormatter
    {
        public string Format(MessagePart part, string value)
        {
            return string.Empty;
        }
    }
}
