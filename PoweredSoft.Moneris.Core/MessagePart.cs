﻿namespace PoweredSoft.Moneris.Core
{
    public class MessagePart
    {
        public string Title { get; set; }

        public IMessagePartFormatter Formatter { get; set; } = new DefaultMessagePartFormatter();

        public IMessagePartValidator Validator { get; set; } = new MessagePartValidator();
    }
}
