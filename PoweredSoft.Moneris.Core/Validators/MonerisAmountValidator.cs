﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PoweredSoft.Moneris.Core.Validators
{
    public class MonerisAmountValidator : MessagePartValidator
    {
        public bool IsRequired { get; set; }

        public MonerisAmountValidator(bool required) : base()
        {
            IsRequired = required;
        }

        public override void Validate(MessagePart part, string value)
        {
            base.Validate(part, value);

            bool isEmpty = string.IsNullOrWhiteSpace(value);

            if (IsRequired && isEmpty)
            {
                this.IsValid = false;
                Errors = new List<string>() { "Value is missing" };
            }

            if (!isEmpty)
            {
                var firstNonDigit = value.Cast<char?>().FirstOrDefault(c => !char.IsDigit(c.Value));
                if (firstNonDigit != null)
                {
                    IsValid = false;
                    Errors = new List<string> { $"{firstNonDigit} in {value} is not a valid digit" };
                }
            }           
        }
    }
}
