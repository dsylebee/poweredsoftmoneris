﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PoweredSoft.Moneris.Core
{
    public class MessagePartResult
    {
        public MessagePartResult(MessagePart part)
        {
            Part = part;
        }

        public MessagePart Part { get; set; }

        public bool IsValid { get; set; }

        public IEnumerable<string> Errors { get; set; }

        public IEnumerable<string> Warnings { get; set; }

        public string RawValue { get; set; }

        public string FormattedValue { get; set; }

        public string ErrorMessage => string.Join("\n", Errors);
        public string WarningMessage => string.Join("\n", Warnings);

        public virtual void SetRawValue(string rawValue)
        {
            RawValue = rawValue;
            Validate();
            Format();
        }

        protected virtual void Format()
        {
            if (!IsValid)
                return;

             FormattedValue = Part.Formatter.Format(Part, RawValue);
        }

        protected virtual void Validate()
        {
            Part.Validator.Validate(Part, RawValue);
            IsValid = Part.Validator.IsValid;
            Errors = Part.Validator.Errors?.ToList() ?? new List<string>();
            Warnings = Part.Validator.Warnings?.ToList() ?? new List<string>();
        }
    }

    public class MessageFormatResult
    {
        public MessageFormatResult(MessageFormat format)
        {
            Format = format;
        }

        public MessageFormat Format { get; set; }
        public List<MessagePartResult> Parts { get; set; }
    }
}
