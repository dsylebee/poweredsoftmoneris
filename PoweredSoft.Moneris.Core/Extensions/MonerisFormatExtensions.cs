﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PoweredSoft.Moneris.Core.Extensions
{
    public static class MonerisFormatExtensions
    {
        public static string TrimEndOfMessage(this string source, string value = "[0x04]")
        {
            if (!source.EndsWith(value))
                return source;

            return source.Remove(source.LastIndexOf(value));
        }

        public static MessageFormatResult Parse(this MessageFormat format, string trace)
        {
            if (format == null)
                throw new ArgumentNullException("format");

            if (string.IsNullOrWhiteSpace(trace))
                throw new ArgumentNullException("trace");

            // the result.
            var result = new MessageFormatResult(format);

            // break the strings.
            var finalTrace = trace.Replace("[0x1C]", "\x1c");
            var parts = finalTrace.Split(new char[] { '\x1c' } , StringSplitOptions.None);

            // validate each parts :]
            result.Parts = format.Parts.Select((part, index) =>
            {
                var mpr = new MessagePartResult(part);
                string rawValue = index < parts.Length ? parts[index] : null;
                mpr.SetRawValue(rawValue);
                return mpr;
            }).ToList();

            return result;
        }
    }
}
