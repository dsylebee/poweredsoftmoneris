﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PoweredSoft.Moneris.Core
{
    public interface IMessagePartValidator
    {
        bool IsValid { get;  }

        IEnumerable<string> Errors { get; }

        IEnumerable<string> Warnings { get; }

        void Validate(MessagePart part, string value);
    }

    public class MessagePartValidator : IMessagePartValidator
    {
        public bool IsValid { get; protected set; }

        public IEnumerable<string> Errors { get; protected set; }

        public IEnumerable<string> Warnings { get; protected set; }

        public virtual void Validate(MessagePart part, string value)
        {
            Reset();
            IsValid = true;
        }

        protected virtual void Reset()
        {
            IsValid = false;
            Errors = new List<string>();
            Warnings = new List<string>();
        }
    }
}
