using System.Collections.Generic;
using PoweredSoft.Moneris.Core;
using PoweredSoft.Moneris.Core.Formatters;
using PoweredSoft.Moneris.Core.Validators;

namespace PoweredSoft.Moneris.PosPad.V2
{
    public class EcrFinancialResponseMSD : MessageFormat
    {
        public override void Initialize()
        {
            base.Initialize();
            Title = "ECR Finacial Response V2.x (MSD Debit card or MSD Credit Card)";
            Parts = new List<MessagePart>()
            {
                new MessagePart { Title = "Condition Code" },
                new MessagePart { Title = "Transaction Code" },
                new MessagePart { Title = "Date/Time", Formatter = new MonerisTimeStampFormatter() },
                new MessagePart { Title = "Amount", Formatter = new MonerisAmountFormatter(), Validator = new MonerisAmountValidator(false) },
                new MessagePart { Title = "Customer Account #", Formatter = new MonerisTimeStampFormatter() },
                new MessagePart { Title = "Expiry Date", Formatter = new MonerisTimeStampFormatter() },
                new MessagePart { Title = "ISO Response Code" },
                new MessagePart { Title = "Moneris Host Response Code" },
                new MessagePart { Title = "Approval Number" },
                new MessagePart { Title = "Unique Transaction ID" },
                new MessagePart { Title = "Language Code" },
                new MessagePart { Title = "ECR ID" },
                new MessagePart { Title = "swipe Indicator" },
                new MessagePart { Title = "Account Type" },
                new MessagePart { Title = "Card Name" },
                new MessagePart { Title = "CVM Indicator" },
                new MessagePart { Title = "SAF Indicator" },
                new MessagePart { Title = "Logon Required" },
                new MessagePart { Title = "Cashback Amount", Formatter = new MonerisAmountFormatter(), Validator = new MonerisAmountValidator(false) },
                new MessagePart { Title = "ECHO Data" }
            };
        }
    }
}