﻿using System;
using System.Collections.Generic;
using System.Text;
using PoweredSoft.Moneris.Core;

namespace PoweredSoft.Moneris.PosPad.V1117
{
    public static class Messages
    {
        public static List<MessageFormat> Formats => new List<MessageFormat>()
        {
            new EcrPurchaseRequest(),
            new EcrFinancialResponse()
        };
    }
}
