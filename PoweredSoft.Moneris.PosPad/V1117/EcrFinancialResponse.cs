using System.Collections.Generic;
using PoweredSoft.Moneris.Core;
using PoweredSoft.Moneris.Core.Formatters;
using PoweredSoft.Moneris.Core.Validators;

namespace PoweredSoft.Moneris.PosPad.V1117
{
    public class EcrFinancialResponse : MessageFormat
    {
        public override void Initialize()
        {
            base.Initialize();
            Title = "ECR Finacial Response";
            Parts = new List<MessagePart>()
            {
                new MessagePart { Title = "Condition Code" },
                new MessagePart { Title = "Transaction Code" },
                new MessagePart { Title = "ECR ID" },
                new MessagePart { Title = "SAF Indicator" },
                new MessagePart { Title = "Moneris Host Response Code" },
                new MessagePart { Title = "ISO Response Code" },
                new MessagePart { Title = "Language Code" },
                new MessagePart { Title = "Date/Time", Formatter = new MonerisTimeStampFormatter() },
                new MessagePart { Title = "Total Amount", Formatter = new MonerisAmountFormatter(), Validator = new MonerisAmountValidator(true) },
                new MessagePart { Title = "Partial Auth Amount", Formatter = new MonerisAmountFormatter(), Validator = new MonerisAmountValidator(false)  },
                new MessagePart { Title = "Available Balance", Formatter = new MonerisAmountFormatter(), Validator = new MonerisAmountValidator(false) },
                new MessagePart { Title = "Tip Amount", Formatter = new MonerisAmountFormatter(), Validator = new MonerisAmountValidator(false) },
                new MessagePart { Title = "Cashback Amount", Formatter = new MonerisAmountFormatter(), Validator = new MonerisAmountValidator(false) },
                new MessagePart { Title = "Surcharge Amount", Formatter = new MonerisAmountFormatter(), Validator = new MonerisAmountValidator(false) },
                new MessagePart { Title = "Foreign Currency Amount", Formatter = new MonerisAmountFormatter(), Validator = new MonerisAmountValidator(false) },
                new MessagePart { Title = "Foreign Currency Code" },
                new MessagePart { Title = "Conversion Rate" },
                new MessagePart { Title = "Markup" },
                new MessagePart { Title = "Card Number (masked)" },
                new MessagePart { Title = "Card Plan" },
                new MessagePart { Title = "Card Name" },
                new MessagePart { Title = "Account Type" },
                new MessagePart { Title = "Mode Of Entry" },
                new MessagePart { Title = "Form FActor" },
                new MessagePart { Title = "CVM Indicator" },
                new MessagePart { Title = "DCC opt-in" },
                new MessagePart { Title = "Reserved Field 2" },
                new MessagePart { Title = "Authorization Code" },
                new MessagePart { Title = "Sequence Number" },
                new MessagePart { Title = "Invoice Number" },
                new MessagePart { Title = "Echo Data" },
                new MessagePart { Title = "Reserved Field 3" },
                new MessagePart { Title = "Reserved Field 4" },
                new MessagePart { Title = "EMV Application ID (AID)" },
                new MessagePart { Title = "Application Label" },
                new MessagePart { Title = "Application Prefered Name" },
                new MessagePart { Title = "ARQC" },
                new MessagePart { Title = "Terminal Verifications Results FOR ARQC" },
                new MessagePart { Title = "TC or AAC" },
                new MessagePart { Title = "Terminal verification results (TVR) for use in the TC OR AAC" },
                new MessagePart { Title = "Transaction STatus Information (TSI)" },
                new MessagePart { Title = "Token Response Code" },
                new MessagePart { Title = "Token" },
                new MessagePart { Title = "Logon Required" },
                new MessagePart { Title = "Encrypted Track2" },
            };
        }
    }
}