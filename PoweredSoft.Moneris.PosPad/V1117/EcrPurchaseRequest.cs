﻿using System.Collections.Generic;
using PoweredSoft.Moneris.Core;
using PoweredSoft.Moneris.Core.Formatters;
using PoweredSoft.Moneris.Core.Validators;

namespace PoweredSoft.Moneris.PosPad.V1117
{
    public class EcrPurchaseRequest : MessageFormat
    {
        public override void Initialize()
        {
            base.Initialize();
            Title = "ECR Purchase Request";
            Parts = new List<MessagePart>()
            {
                new MessagePart { Title = "Transaction Code" },
                new MessagePart { Title = "Amount", Formatter = new MonerisAmountFormatter(), Validator = new MonerisAmountValidator(true) },
                new MessagePart { Title = "Token Request"},
                new MessagePart { Title = "Token" },
                new MessagePart { Title = "Card Plan" },
                new MessagePart { Title = "Entry Method" },
                new MessagePart { Title = "Invoice Number" },
                new MessagePart { Title = "Echo Data"},
                new MessagePart { Title = "MOTO E-Commerce Flag" },
                new MessagePart { Title = "Original Approval Number" },
                new MessagePart { Title = "Promo Code" },
                new MessagePart { Title = "Mode Of Entry" }
            };
        }
    }
}
