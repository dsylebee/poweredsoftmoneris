﻿using PoweredSoft.Moneris.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoweredSoft.Moneris.MessageViewer.Models
{
    public class MainWindowViewModel
    {
        public List<MessageFormat> Formats => PosPad.V1117.Messages.Formats.Union(PosPad.V2.Messages.Formats).ToList();
        public MessageFormat SelectedMessageFormat { get; set; }
        public MessageFormatResult CurrentMessageFormatResult { get; set; }
    }
}
