﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PoweredSoft.Moneris.MessageViewer.Models;
using PoweredSoft.Moneris.Core.Extensions;

namespace PoweredSoft.Moneris.MessageViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowViewModel ViewModel { get; set; } = new MainWindowViewModel();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = ViewModel;
        } 

        private void BtnGo_Click(object sender, RoutedEventArgs e)
        {
            
            if (ViewModel.SelectedMessageFormat == null)
            {
                MessageBox.Show("Must first select a message format");
                return;
            }

            if (string.IsNullOrWhiteSpace(TxtLog.Text))
            {
                MessageBox.Show("Log must not be empty");
                return;
            }

            var message = TxtLog.Text;

            var without0x04 = message.TrimEndOfMessage("[0x04]");

            ViewModel.CurrentMessageFormatResult = ViewModel.SelectedMessageFormat.Parse(without0x04);
            GridMessageParts.ItemsSource = ViewModel.CurrentMessageFormatResult.Parts;
        }
    }
}
